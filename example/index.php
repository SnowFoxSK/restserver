<?php

require __DIR__ . '/../source/snowfox/RestServer/RestServer.php';
require 'TestController.php';

$server = new \snowfox\RestServer\RestServer('debug');
$server->addClass('TestController');
$server->handle();
